package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarComparator implements Comparator<Car> {

	@Override
	public int compare(Car o1, Car o2) {
		
		//If the cars are equal, return 0;
		if(o1.equals(o2)) {
			return 0;
		}
		//If the brands are equals check the models
		else if(o1.getCarBrand().toLowerCase().compareTo(o2.getCarBrand().toLowerCase()) == 0) {
			//If the models are equal, compare the Model Options
			if(o1.getCarModel().toLowerCase().compareTo(o2.getCarModel().toLowerCase()) == 0) {
				//Return the comparison between the Model Options
				return o1.getCarModelOption().toLowerCase().compareTo(o2.getCarModelOption().toLowerCase());
			}
			//Return the comparison of the Models.
			else {
				return o1.getCarModel().toLowerCase().compareTo(o2.getCarModel().toLowerCase());
			}
		}
		//Return the comparison of the Brands.
		else {
			return o1.getCarBrand().toLowerCase().compareTo(o2.getCarBrand().toLowerCase());
		}
	}


}
