package edu.uprm.cse.datastructures.cardealer.util;

import edu.uprm.cse.datastructures.cardealer.model.LongComparator;

public class testTreeInsertion {

	public static void main(String[] args) {
		TwoThreeTree<Long, Long> T = new TwoThreeTree<Long, Long>(new LongComparator());
		T.put((long)3, (long)3);
		T.put((long)1, (long)1);
		T.put((long)2, (long)2);
		T.put((long)10, (long)10);
		T.put((long)12, (long)12);
		T.put((long)20, (long)20);
		T.put((long)100, (long)100);
		T.put((long)5, (long)5);
		T.put((long)7, (long)7);
		T.put((long)8, (long)8);
		T.print();
	}

}
