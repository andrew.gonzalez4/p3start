//IMPORTS
package edu.uprm.cse.datastructures.cardealer.model;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
import edu.uprm.cse.datastructures.cardealer.util.HashTableOA;
import edu.uprm.cse.datastructures.cardealer.util.SortedList;
import edu.uprm.cse.datastructures.cardealer.util.TwoThreeTree;  

@Path("/cars")
public class CarManager {


	private TwoThreeTree<Long,Car> cTree = CarTree.getInstance(); 
	//Get Method
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {
		CircularSortedDoublyLinkedList<Car> cArray = new CircularSortedDoublyLinkedList<Car>(new CarComparator());
		Car[] result  = new Car[cTree.getValues().size()];
		for (int i = 0; i < cTree.getValues().size(); i++) {
			cArray.add(cTree.getValues().get(i));
		}

		for (int i = 0; i < cArray.size(); i++) {
			result[i] = cArray.get(i);
		}

		return result;

	} 
	//Get Method with the ID
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id){
		Car result = cTree.get(id);
		if(result != null) {
			return result;
		}

		else {
			throw new NotFoundException();
		}
	}      

	//Adding a car
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car obj){
		if(cTree.contains(obj.getCarId())) {
			return Response.status(Response.Status.CONFLICT).build();
		}
		cTree.put(obj.getCarId(), obj);
		return Response.status(201).build();
	}
	//Updating a car with a given ID.
	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCustomer(Car obj){

		if(cTree.contains(obj.getCarId())) {
			cTree.remove(obj.getCarId());
			cTree.put(obj.getCarId(), obj);
			return Response.status(200).build();
		}

		else {
			return Response.status(404).build();
		}
	}     
	//Deleting a car with a given ID.
	@DELETE
	@Path("/{id}/delete")
	public Response deleteCar(@PathParam("id") long id){

		if(cTree.contains(id)) {
			cTree.remove(id);
			return Response.status(Response.Status.OK).build();
		}
		//If you didn't found a car with the ID return not found.
		return Response.status(Response.Status.NOT_FOUND).build(); 

	}
}      


