package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.util.TwoThreeTree;

public class CarTree {
	
	private static TwoThreeTree<Long, Car> cTree;
	private static CarTree Singleton = new CarTree();
	
	private CarTree() {
		cTree =  new TwoThreeTree<Long,Car>(new LongComparator());
	}
	
	public static TwoThreeTree<Long, Car> getInstance(){
		return cTree;
	}
	
	public static void resetCars(){
		cTree = new TwoThreeTree<Long,Car>(new LongComparator());
	}
	
}
