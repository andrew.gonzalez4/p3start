package edu.uprm.cse.datastructures.cardealer.util;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TwoThreeTree<K, V> extends BTree<K, V>{

	//Constructor which comes from the BTree.
	public TwoThreeTree(Comparator<K> keyComparator) {
		super(keyComparator);
		// TODO Auto-generated constructor stub
	}

	//The size of a TwoThreeTree is the current size minus the deleted elements.
	@Override
	public int size() {
		return this.currentSize - this.deleted;
	}

	//Just return if the size is 0.
	@Override
	public boolean isEmpty() {
		return this.size() == 0;
	}

	//First check if the root is not null, if it is not null, check the Mapentry and return the value of that Mapentry.
	@Override
	public V get(K key) {
		MapEntry dummy = new MapEntry(key,null,this.keyComparator);
		if(this.root == null) {
			return null;
		}

		else {
			MapEntry entry = this.getAux(super.root, key);
			if(entry == null) return null;
			if(entry.deleted) return null;
			return entry.value;
		}
	}

	//For the getAux recursively traverse through the Tree to see if a node with the given key exists.
	public MapEntry getAux(TreeNode N, K key) {
		MapEntry newIns = new MapEntry(key,null,this.keyComparator);
		if(N == null) {
			return null;
		}
		else if(newIns.compareTo(N.entries.first()) == 0) {
			return N.entries.first();
		}
		if(newIns.compareTo(N.entries.last()) == 0) {
			return N.entries.last();
		}
		if(newIns.compareTo(N.entries.first()) < 0) {
			return getAux(N.left,key);
		}
		if(N.entries.size() == 1 || newIns.compareTo(N.entries.last()) < 0) {
			return getAux(N.center,key);
		}

		if(newIns.compareTo(N.entries.first()) > 0 && newIns.compareTo(N.entries.first()) < 0) {
			return getAux(N.center,key);
		}
		if(newIns.compareTo(N.entries.last()) < 0) {
			return getAux(N.center,key);
		}
		else {
			return getAux(N.right,key);
		}
	}

	//For the put, the insertion is almost the same for a BST just that we have to split if the size of the entries is >=3.
	@Override
	public V put(K key, V value) {
		if(key == null || value == null) {
			return null;
		}
		MapEntry newIns = new MapEntry(key, value, this.keyComparator);
		if(super.root == null) {
			super.root = new TreeNode(newIns, null, this.keyComparator);
			super.currentSize++;
			return value;
		}
		putAux(super.root,key,value,newIns);
		return value;
	}

	//For the put Aux, recursively traverse until the node where the key belongs is found. After adding, the size of the entries must be checked in order to know if a split is required.
	public void putAux(TreeNode N, K key, V value, MapEntry newIns) {
		if(N == null) {
			return;
		}

		if(N.entries.first().key.equals(key)) {
			if(N.entries.first().deleted) {
				this.deleted--;
			}
			N.entries.remove(N.entries.first());
			N.entries.add(newIns);

			return;
		}

		if(N.entries.last().key.equals(key)) {
			if(N.entries.last().deleted) {
				this.deleted--;
			}
			N.entries.remove(N.entries.last());
			N.entries.add(newIns);
			return;
		}
		if(this.isLeaf(N)) {
			N.entries.add(newIns);
			super.currentSize++;
			if(N.entries.size() <= 2) {
				return;
			}
			this.split(N);
		}

		if(N.entries.size() == 1) {
			if(newIns.compareTo(N.entries.first()) < 0) {
				putAux(N.left, key,value,newIns);
				return;
			}

			else {
				putAux(N.center, key,value,newIns);
				return;
			}
		}

		else {
			if(newIns.compareTo(N.entries.first()) < 0) {
				putAux(N.left, key,value,newIns);
				return;
			}
			if(newIns.compareTo(N.entries.last()) < 0 && newIns.compareTo(N.entries.first()) >0) {
				putAux(N.center, key, value, newIns);
				return;
			}
			else {
				putAux(N.right, key, value, newIns);
				return;
			}
		}

	}

	//For the remove, the MapEntry is marked as deleted instead of actually removing it for an easier approach.
	@Override
	public V remove(K key) {
		MapEntry desired = this.getAux(super.root, key);
		if(desired == null) {
			return null;
		}
		desired.deleted = true;
		super.deleted++;
		return desired.value;
	}

	//If the get returns something other than null the tree contains the key given.
	@Override
	public boolean contains(K key) {
		return this.get(key) != null;
	}

	//For the getKeys traverse through the tree grabbing the keys of the MapEntries.
	@Override
	public List<K> getKeys() {
		List<K> L = new ArrayList<K>();
		List<MapEntry> M = new ArrayList<MapEntry>();
		if(this.root == null) {
			return L;
		}
		getKeysValuesAux(M, super.root);
		for (int i = 0; i <M.size(); i++) {
			L.add(M.get(i).key);
		}
		return L;
	}

	//For the getValues traverse through the tree grabbing the values of the MapEntries.
	@Override
	public List<V> getValues() {
		List<V> L = new ArrayList<V>();
		List<MapEntry> M = new ArrayList<MapEntry>();
		if(this.root == null) {
			return L;
		}
		getKeysValuesAux(M, super.root);
		for (int i = 0; i <M.size(); i++) {
			L.add(M.get(i).value);
		}
		return L;
	}

	//I decided to make the KeysandValues aux the same since I can then have less code for the same things I wanted to get.
	public void getKeysValuesAux(List<MapEntry> M, TreeNode N) {
		if(N == null) {
			return;
		}
		else {
			getKeysValuesAux(M, N.left);
			if(!N.entries.first().deleted) {
				M.add(N.entries.first());
			}
			getKeysValuesAux(M, N.center);
			if(N.entries.size() > 1) {
				if(!N.entries.last().deleted) {
					M.add(N.entries.last());
				}
			}
			getKeysValuesAux(M, N.right);
		}
	}

	//To see if a Node is leaf, its children must be null.
	@Override
	boolean isLeaf(BTree<K, V>.TreeNode treeNode) {
		if((treeNode.left == null) && (treeNode.right == null) && (treeNode.center == null)) {
			return true;
		}
		return false;
	}

	//For the split method, the goal is to only have 2 elements per node, if this is not the case then the split will act.
	@Override
	void split(BTree<K, V>.TreeNode treeNode) {

		//If the size is less than 3 the split has finished.
		if(treeNode.entries.size() < 3) {
			return;
		}

		//If the parent of the current node is null, the center element of the node will be the root now.
		if(treeNode.parent == null) {
			TreeNode newRoot = new TreeNode(treeNode.entries.get(1), null, this.keyComparator);
			TreeNode leftC = new TreeNode(treeNode.entries.first(), null, this.keyComparator);
			TreeNode centerC = new TreeNode(treeNode.entries.last(), null, this.keyComparator);

			leftC.parent = newRoot;
			centerC.parent = newRoot;
			newRoot.left =leftC;
			newRoot.center = centerC;
			//Condicionales
			if(treeNode.left != null) {
				leftC.left = treeNode.left;
				treeNode.left.parent = leftC;
			}
			if(treeNode.center != null) {
				leftC.center = treeNode.center;
				treeNode.center.parent = leftC;
			}
			if(treeNode.right != null) {
				centerC.left = treeNode.right;
				treeNode.right.parent = centerC;
			}
			if(treeNode.temp != null) {
				centerC.center = treeNode.temp;
				treeNode.temp.parent = centerC;
			}
			super.root = newRoot;
			return;
		}

		treeNode.parent.entries.add(treeNode.entries.get(1));
		treeNode.entries.remove(1);
		TreeNode leftC = new TreeNode(treeNode.entries.first(), null, this.keyComparator);
		TreeNode centerC = new TreeNode(treeNode.entries.last(), null, this.keyComparator);
		leftC.parent = treeNode.parent;
		centerC.parent = treeNode.parent;

		if(treeNode.left != null) {
			leftC.left = treeNode.left;
			treeNode.left.parent = leftC;
		}
		if(treeNode.center != null) {
			leftC.center = treeNode.center;
			treeNode.center.parent = leftC;
		}
		if(treeNode.right != null) {
			centerC.left = treeNode.right;
			treeNode.right.parent = centerC;
		}
		if(treeNode.temp != null) {
			centerC.center = treeNode.temp;
			treeNode.temp.parent = centerC;
		}

		if(leftC.entries.first().compareTo(treeNode.parent.center.entries.last()) > 0) {
			treeNode.parent.right = leftC;
			treeNode.parent.temp = centerC;
		}
		else if(leftC.entries.first().compareTo(treeNode.parent.left.entries.last())>0) {
			TreeNode tem = treeNode.parent.right;
			treeNode.parent.center = leftC;
			treeNode.parent.right = centerC;
			treeNode.parent.temp = tem;
		}
		else{
			TreeNode temMid = treeNode.parent.center;
			TreeNode temRight = treeNode.parent.right;
			treeNode.parent.left = leftC;
			treeNode.parent.center = centerC;
			treeNode.parent.right = temMid;
			treeNode.parent.temp = temRight;
		}

		this.split(treeNode.parent);

	}

}
